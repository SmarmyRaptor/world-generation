#include "Vector2D.h"
#include<cstdlib>

Vector2D::Vector2D(float x_val, float y_val){
	x = x_val;
	y = y_val;
}
Vector2D::Vector2D(const Vector2D& v){
	x = v.getX();
	y = v.getY();
}

Vector2D Vector2D::addVectors(const Vector2D& v1, const Vector2D& v2){
	return Vector2D(v1.getX() + v2.getX(), v1.getY() + v2.getY());
}
Vector2D Vector2D::operator+(const Vector2D& v){
	return addVectors(*this, v);
}
Vector2D Vector2D::subVectors(const Vector2D& v1, const Vector2D& v2){
	return Vector2D(v1.getX() - v2.getX(), v1.getY() - v2.getY());
}
Vector2D Vector2D::operator-(const Vector2D& v){
	return subVectors(*this, v);
}
Vector2D Vector2D::multVectors(const Vector2D& v1, const Vector2D& v2){
	return Vector2D(v1.getX() * v2.getX(), v1.getY() * v2.getY());
}
Vector2D Vector2D::operator*(const Vector2D& v){
	return multVectors(*this, v);
}
Vector2D Vector2D::scaleVector(const Vector2D& v1, float scale_amount){
	return Vector2D(v1.getX() * scale_amount, v1.getY() * scale_amount);
}
Vector2D Vector2D::operator*(float val){
	return scaleVector(*this, val);
}

inline float Vector2D::dot(const Vector2D& v)const{
	return v.getX()*x + v.getY()*y;
}

inline void Vector2D::add(const Vector2D& v){
	x += v.getX();
	y += v.getY();
}
inline void Vector2D::add(float x_val, float y_val){
	x += x_val;
	y+= y_val;
}

inline void Vector2D::sub(const Vector2D& v){
	x-= v.getX();
	y-= v.getX();
}
inline void Vector2D::sub(float x_val, float y_val){
	x -= x_val;
	y -= y_val;
}	
		
inline void Vector2D::mult(const Vector2D& v){
	x *= v.getX();
	y *= v.getY();
}
inline void Vector2D::mult(float x_val, float y_val){
	x *= x_val;
	y *= y_val;
}
		
inline void Vector2D::scale(float val){
	x *= val;
	y *= val;
}

bool Vector2D::operator==(const Vector2D& v)const{
	return (v.getX() == x && v.getY() == y);
}

std::ostream& operator<<(std::ostream& os, const Vector2D& vec){
	os<<"x: "<<vec.getX()<<", y:"<<vec.getY();
	return os;
}