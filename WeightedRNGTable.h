#ifndef WEIGHTED_RNG_TABLE
#define WEIGHTED_RNG_TABLE

#include<cstdlib>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <vector>
#include "isaac.hpp"

template <typename T>
class WeightedRNGTable{
	template <class U>
		friend std::ostream& operator<<(std::ostream& os, const WeightedRNGTable<U>& table);
	
	private:
		unsigned int total_weight;
		
		struct ObjectInfo{
			unsigned int weight;
			unsigned int ceiling;
			unsigned int floor;
			T *object;
			
			~ObjectInfo(){
			}
		};
		
		static constexpr double rand_num_size = 4294967295LL;
		std::vector<ObjectInfo> stored_objects;
		QTIsaac<8, ISAAC_INT> rng;
		
		//binary search the table for the object that falls under that (total)weight in the table
		T* bsTable(unsigned int search_value, unsigned int start_loc, unsigned int end_loc);
		double generateDecimal();

	protected: 
		  std::vector<ObjectInfo> getStoredObjects() const{return &stored_objects;}
	public:
		WeightedRNGTable(){};
		WeightedRNGTable(T *object, unsigned int weight, unsigned int seed1, unsigned int seed2, unsigned int seed3);
		//The copied table will have a different RNG
		WeightedRNGTable(const WeightedRNGTable& table);
		
		
		unsigned int  getTotalWeight()const{return total_weight;}
		
		void addValue(T *object, unsigned int weight);
		T* rollTable(float table_skew = 0);
		//sorts table by ascending weight
		void sortTableByWeight();
		
		//compares two objects, returning true if obj1 is heaveier than obj2
		static bool objectLighter(ObjectInfo& obj1, ObjectInfo& obj2);
		
		//compares two objects using the > fucntion of the template object
		static bool objectGreater(ObjectInfo& obj1, ObjectInfo& obj2);
		
		~WeightedRNGTable(){}
};

//I hate template classes with g++
template <typename T> T* WeightedRNGTable<T>::bsTable(unsigned int search_value, unsigned int start_loc, unsigned int end_loc){
	int curr_loc = int((end_loc-start_loc)/2 + start_loc); //avoid overflow
	if(search_value >= stored_objects[curr_loc].ceiling){
		return bsTable(search_value, curr_loc+1, end_loc);
	}else if(search_value< stored_objects[curr_loc].floor){
		return bsTable(search_value, start_loc, curr_loc-1);
	}
	return stored_objects[curr_loc].object;
}
template <typename T> double WeightedRNGTable<T>::generateDecimal(){
	unsigned int rand = (unsigned int)(rng.rand());
	return rand%total_weight;
}
template <typename T>  bool WeightedRNGTable<T>::objectLighter(ObjectInfo& obj1, ObjectInfo& obj2){
	return obj1.weight > obj2.weight;
}
template <typename T>  bool WeightedRNGTable<T>::objectGreater(ObjectInfo& obj1, ObjectInfo&obj2){
	return true;
}

template <typename T> WeightedRNGTable<T>::WeightedRNGTable(T* object, unsigned int weight, unsigned int seed1, unsigned int seed2, unsigned int seed3){
	ObjectInfo temp = {
		weight, //weight
		weight,//ceiling
		0,//floor
		object, //object
	};
	stored_objects.push_back(temp);
	total_weight = weight;
	rng.srand(seed1, seed2, seed3, nullptr);
}
template <typename T> WeightedRNGTable<T>::WeightedRNGTable(const WeightedRNGTable& table){
	std::vector<ObjectInfo>* oldData = table.getStoredObjects();
	ObjectInfo temp = {
		oldData[0].weight, //weight
		oldData[0].weight,//ceiling
		0,//floor
		oldData[0].object, //object
	};
	stored_objects.push_back(temp);
	for(unsigned int i = 0; i < oldData.size(); i++){
		addValue(oldData[i].object, oldData[i].weight);
	}
	
}
template <typename T> void WeightedRNGTable<T>::addValue(T* object, unsigned int weight){
	ObjectInfo last_obj = stored_objects.back();
	
	ObjectInfo temp = {
		weight, //weight
		weight + last_obj.ceiling,//ceiling
		last_obj.ceiling,//floor
		object, //object
	};
	
	total_weight += weight;
	
	stored_objects.push_back(temp);
}

template <typename T> T* WeightedRNGTable<T>::rollTable(float table_skew){
	int search_value = int(generateDecimal() * (1+table_skew));
	if(search_value > total_weight-1){
		search_value = total_weight-1;
	}
	return  bsTable(search_value, 0, stored_objects.size());
}

template <typename T> void WeightedRNGTable<T>::sortTableByWeight(){
	std::sort(stored_objects.begin(), stored_objects.end(), &objectLighter);
	
	int prev_weight = 0;
	for(unsigned int i =0; i<stored_objects.size(); i++){
		stored_objects[i].floor = prev_weight;
		prev_weight += stored_objects[i].weight;
		stored_objects[i].ceiling = prev_weight;
	}
}

template <typename T> std::ostream& operator<<(std::ostream& os, const WeightedRNGTable<T>& table){
	os<<"Weight\tObject\n";
	for(unsigned int i =0; i<table.stored_objects.size(); i++){
		os<<table.stored_objects[i].weight<<"\t"<<table.stored_objects[i].floor<<"\t"<<table.stored_objects[i].ceiling<<"\t"<<*(table.stored_objects[i].object)<<std::endl;
	}
	os<<"\n";
	return os;
}

#endif