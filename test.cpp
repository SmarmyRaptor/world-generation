#include "StringParse.h"
#include "GameRegion.h"
#include <iostream>
#include <fstream>


int main(){
	char a[2] = {'<', '>'};
	std::vector<std::string> v;
	std::string  test_string = "<<<><test this string>";
	StringParse::strip(a, test_string, 2);
	v = StringParse::split(test_string, ' ');
	std::cout<<test_string<<"\n";
	std::cout<<v[0]<<","<<v[1]<<","<<v[2]<<"\n---------------------------------\n";
	
	//input string format: id	wieght	sea	<tilex,tiley> <tilex,tiley>	<edgex,edgey> <edgex,edgey>
	std::string test_continent = "6\t2\t1\t<3,3> <4,4> <5,5> <6,6>\t<3,2> <2,3> <3,4> <4,3>";
	GameContinent con1(test_continent);
	std::cout<<con1<<"\n"<<test_continent<<"\n";
}