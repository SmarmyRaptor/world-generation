#include "StringParse.h"


std::vector<std::string>& StringParse::split(const std::string &splitee, char delimitor, std::vector<std::string> &to_vec){
	std::stringstream string_stream(splitee);
	std::string part;
	while(std::getline(string_stream, part, delimitor)){
		if(!part.empty()){
			to_vec.push_back(part);
		}
	}
	return to_vec;
}

std::vector<std::string>  StringParse::split(const std::string &splitee, char delimitor){
	std::vector<std::string> ret;
	split(splitee, delimitor, ret);
	return ret;
}
//Change to accept an array of characters instead of just one
std::string& StringParse::strip(char strip_char, std::string &strip_string){
	char char_array[1] = {strip_char};
	return strip(char_array, strip_string, 1);
}

std::string& StringParse::strip(std::string &strip_string){
	char char_array[3] = {'\n', ' ', '\t'};
	return strip(char_array, strip_string, 3);
}
std::string& StringParse::strip(char* strip_chars, std::string &strip_string, unsigned int size){
	std::string::iterator start = strip_string.begin();
	while(start != strip_string.end() && charIsIn(strip_chars, *start, size)){
		start = strip_string.erase(start);
	}
	std::string::iterator end = strip_string.end();
	end--;
	if(!strip_string.empty()){
		while(end != strip_string.begin() && charIsIn(strip_chars, *end, size)){
			strip_string.erase(end--);
		}
	}
	return strip_string;
}


bool StringParse::charIsIn(char* char_array, char test_char, unsigned int size){
	for(unsigned int i =0; i<size; i++){
		if(char_array[i] == test_char){
			return true;
		}
	}
	return false;
}



