#include "WeightedRNGTable.h"
#include "Vector2D.h"
#include "Grid2D.h"
#include "GameRegion.h"
#include "RandomTools.h"
#include "StringParse.h"
#include <ostream>
#include <stdlib.h>


//vector accessor of grid2D
//virtual set and isfree for grid2d

int main(int argc, char* argv[]){
	std::cout<<argv[0]<<"\n";
	
	int seed = 101;
	int num_continents = 50;
	int map_width = 100;
	int map_height = 50;
	int sea_percentage = 75;
	int continent_variance = 10;
	
	srand(seed);

	Grid2DWrap<bool> continent_grid(map_width, map_height);
 
	int* starting_pos_raw = new int[num_continents];
	Vector2D* pos_vector = new Vector2D[num_continents];
	GameContinent* continents = new GameContinent[num_continents];
		
	//get a bunch of random starting points and turn them into vectors
	RandomTools::smallUniqueSet(starting_pos_raw, num_continents, map_width*map_height, 0);
	for(unsigned int i = 0; i<num_continents; i++){
		pos_vector[i] = continent_grid.vectorize(starting_pos_raw[i]);
	}
	
	delete[] starting_pos_raw;
	int num_sea = (num_continents *sea_percentage)/100;
	
	
	unsigned int curr_weight;
	for(unsigned int i = 0; i<num_sea; i++){
		curr_weight = 100 +((rand()%(continent_variance*2))-continent_variance);
		continents[i] = GameContinent(true, curr_weight, rand());
	}
	for(unsigned int i = num_sea; i<num_continents; i++){
		curr_weight = 100 +((rand()%(continent_variance*2))-continent_variance);
		continents[i] = GameContinent(false, curr_weight, rand());
	}
	
	WeightedRNGTable<GameContinent> continent_table(&continents[0], continents[0].weight, rand(), rand(), rand());
	for(unsigned int i = 1; i<num_continents; i++){
		continent_table.addValue(&continents[i], continents[i].weight);
	}
	continent_table.sortTableByWeight();
	//init tiles in continents
	//init edges in continents
	bool truthyness = true;
	for(unsigned int i = 0; i<num_continents; i++){
		continents[i].addTile(pos_vector[i]);
		continent_grid.set(truthyness, pos_vector[i].getX(), pos_vector[i].getY());
	}
	for(unsigned int i = 0; i<num_continents; i++){
		continents[i].setOpenTileEdges(pos_vector[i], continent_grid);
		std::cout<<continents[i];
	}
	delete[] pos_vector;
	int num_tiles_done = num_continents;
	GameContinent* rand_continent;
	Vector2D rand_edge;
	
	
	while(num_tiles_done <map_height*map_width){
		// get random continent
		rand_continent = continent_table.rollTable();
		// selected continent has open edges
		if(rand_continent->hasEdges()){
			// get random edge from continent
			rand_edge = rand_continent->getRandomEdge();
			// set that tile, mark as taken in grid
			rand_continent->addTile(rand_edge);
			
			continent_grid.set(truthyness, rand_edge.getX(), rand_edge.getY());
			
			
			// remove that edge from all other continents
			for(unsigned int i = 0; i< num_continents; i++){
				continents[i].removeEdge(rand_edge);
			}
			// get open edges for that tile
			rand_continent->setOpenTileEdges(rand_edge, continent_grid);
			num_tiles_done++;
		}
	}
	
	Grid2DWrap<bool> print_grid(map_width, map_height);
	for(unsigned int i = 0; i<num_continents; i++){
		for(unsigned int j =0; j<continents[i].tiles.size(); j++){
			print_grid.set(continents[i].sea_plate, continents[i].tiles[j].getX(),  continents[i].tiles[j].getY());
		}
		
	}
	
	std::cout<<print_grid<<"\n";
	return 0;
}

//g++ -std=c++11 -o test Vector2D.cpp ContinentGenerator.cpp GameRegion.cpp RandomTools.cpp StringParse.cpp -I I:\Game Dev\Code\C++Libs