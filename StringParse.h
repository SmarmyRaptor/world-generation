#ifndef STRING_PARSE
#define STRING_PARSE

#include <string>
#include <sstream>
#include <vector>

namespace StringParse{
	std::vector<std::string>& split(const std::string &splitee, char delimitor, std::vector<std::string> &to_vec);
	std::vector<std::string> split(const std::string &splitee, char delimitor);
	
	std::string &strip(char strip_char, std::string &strip_string);
	//strips whitespace characters
	std::string &strip(std::string &strip_string);
	std::string &strip(char* strip_chars, std::string &strip_string, unsigned int size);
	
	
	bool charIsIn(char* char_array, char test_char, unsigned int size);

}

#endif