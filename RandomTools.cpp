#include "RandomTools.h"
#include <stdlib.h>


//THESE ARE NOT CRYTOPGRAPHICALLY SAFE!!!!!!!!!!
void RandomTools::smallUniqueSet(int* set, unsigned int amount, int ceiling, int floor){
	int valid_count = 0;
	int new_pos;
	while(valid_count<amount){
		new_pos = rand()%(ceiling-floor)+floor;
		if(!isUnique(set, valid_count, new_pos)){
			set[valid_count] = new_pos;
			valid_count++;
		}
	}
}

bool RandomTools::isUnique(int* values, unsigned int len, int new_num){
	for(unsigned int i = 0; i< len; i++){
		if(values[i] == new_num){
			return true;
		}
	}
	return false;
}