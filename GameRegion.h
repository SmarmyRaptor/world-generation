#include "Vector2D.h"
#include "Grid2D.h"
#include <iostream>
#include <vector>
#include <string>
class GameRegion{
	public:
		std::vector<Vector2D> tiles;
		std::vector<Vector2D> edges;
		
		GameRegion(){};
		~GameRegion(){};
		
		bool isInEdges(Vector2D v);
		bool removeEdge(Vector2D i);
		
		Vector2D getRandomEdge();
		template <typename T> void setOpenTileEdges(Vector2D& tile, Grid2DWrap<T>& open_grid);
		inline bool hasEdges(){return edges.size() >0;}
		
		void addTile(Vector2D tile);
		void addEdge(Vector2D edge);
		//template <bool> void addTile(Vector2D& tile, Grid2DWrap<bool>& open_grid);
};

class GameContinent:public GameRegion{
	friend std::ostream& operator<<(std::ostream& os, const GameContinent& gc);
	
	public:
		GameContinent(){}
		GameContinent(bool sea, int weight, int new_id);
		//create continent object from a string containing all the data
		GameContinent(std::string input_string);
		~GameContinent(){};
	
		bool sea_plate;
		int weight;
		int id; 
};
//g++ and templates again
template <typename T> void GameRegion::setOpenTileEdges(Vector2D& tile, Grid2DWrap<T>& open_grid){
	Vector2D up = tile + Vector2D(0, -1);
	up = Vector2D((int)up.getX()%open_grid.getWidth(),(int)up.getY()%open_grid.getHeight());
	Vector2D down = tile + Vector2D(0, 1);
	down = Vector2D((int)down.getX()%open_grid.getWidth(),(int)down.getY()%open_grid.getHeight());
	Vector2D left = tile + Vector2D(-1, 0);
	left = Vector2D((int)left.getX()%open_grid.getWidth(),(int)left.getY()%open_grid.getHeight());
	Vector2D right = tile + Vector2D(1, 0);
	right = Vector2D((int)right.getX()%open_grid.getWidth(),(int)right.getY()%open_grid.getHeight());
	if(open_grid.isFree(up) && !isInEdges(up)){
		edges.push_back(Vector2D(up));
	}
	if(open_grid.isFree(down) && !isInEdges(down))
		edges.push_back(Vector2D(down));
	if(open_grid.isFree(left) && !isInEdges(left))
		edges.push_back(Vector2D(left));
	if(open_grid.isFree(right) && !isInEdges(right))
		edges.push_back(Vector2D(right));
}
//this works, but there is no reason to use it. Easier to change the 
// template <bool> void GameRegion::addTile(Vector2D& tile, Grid2DWrap<bool>& open_grid){
	// addTile(tile);
	// open_grid.getElement(tile) = false;
	// setOpenTileEdges(tile, open_grid);
// }