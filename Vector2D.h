#ifndef VECTOR_2D
#define VECTOR_2D

#include <ostream>

class Vector2D{
	friend std::ostream& operator<<(std::ostream& os, const Vector2D& vec);
	
	protected:
		float x;
		float y;
		
	public:
		Vector2D(){};
		Vector2D(float x_val, float y_val);
		Vector2D(const Vector2D& v);
		
		static  Vector2D addVectors(const Vector2D& v1, const Vector2D& v2);
		Vector2D operator+(const Vector2D& v);
		static  Vector2D subVectors(const Vector2D& v1, const Vector2D& v2);
		Vector2D operator-(const Vector2D& v);
		//separated componants multiplied
		static  Vector2D multVectors(const Vector2D& v1, const Vector2D& v2);
		Vector2D operator*(const Vector2D& v);
		static  Vector2D scaleVector(const Vector2D& v, float scale_amount);
		Vector2D operator*(float val);

		float dot(const Vector2D& v)const;
		
		inline float getX()const{return x;} ;
		inline float getY()const{return y;} ;
		
		void add(const Vector2D& v);
		void add(float x_val, float y_val);	

		void sub(const Vector2D& v);
		void sub(float x_val, float y_val);	
		
		void mult(const Vector2D& v);
		void mult(float x_val, float y_val);
		
		void scale(float val);
		
		bool operator==(const Vector2D& v)const;
				
};

#endif