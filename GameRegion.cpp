#include "GameRegion.h"
#include "StringParse.h"
#include "Grid2D.h"

bool GameRegion::isInEdges(Vector2D v){
	for (unsigned int i = 0; i<edges.size(); i++){
		if(v == edges[i])
			return true;
	}
	return false;
}

bool GameRegion::removeEdge(Vector2D v){
	for (auto i = edges.cbegin(); i!=edges.cend(); i++){
		if(v == *i){
			edges.erase(i);
			return true;
		}
	}
	return false;
}

Vector2D GameRegion::getRandomEdge(){
	int random_edge = rand()%edges.size();
	return Vector2D(edges[random_edge].getX(), edges[random_edge].getY());
}
void GameRegion::addTile(Vector2D tile){
	Vector2D new_tile = Vector2D(tile);
	tiles.push_back(Vector2D(tile.getX(), tile.getY()));
}
void GameRegion::addEdge(Vector2D edge){
	Vector2D new_edge = Vector2D(edge);
	edges.push_back(Vector2D(edge.getX(), edge.getY()));
}

GameContinent::GameContinent(bool sea, int spawn_weight, int new_id){
	sea_plate = sea;
	weight = spawn_weight;
	id = new_id;
};
GameContinent::GameContinent(std::string input_string){
	std::vector<std::string> part_vector = StringParse::split(input_string, '\t');
	id = std::stoi(part_vector[0]);
	weight = std::stoi(part_vector[1]);
	sea_plate = part_vector[2][0] == '1'; //true if sea value is 1, false otherwise
	
	std::vector<std::string> new_tiles = StringParse::split(part_vector[3], ' ');
	std::vector<std::string> new_tile;
	char strip_chars[2] = {'<', '>'};
	
	for (auto i = new_tiles.begin(); i!= new_tiles.end(); i++){
		new_tile = StringParse::split(StringParse::strip(strip_chars, *i, 2), ',');
		addTile(Vector2D(std::stoi(new_tile[0]), std::stoi(new_tile[1])));
	}
	
	std::vector<std::string> new_edges = StringParse::split(part_vector[4], ' ');
	std::vector<std::string> new_edge;
	
	for (auto i = new_edges.begin(); i!= new_edges.end(); i++){
		new_edge = StringParse::split(StringParse::strip(strip_chars, *i, 2), ',');
		addEdge(Vector2D(std::stoi(new_edge[0]), std::stoi(new_edge[1])));
	}
}

std::ostream& operator<<(std::ostream& os, const GameContinent& gc){
	os<<gc.id<<"\t"<<gc.weight<<"\t"<<gc.sea_plate<<"\t";
	for(unsigned int i = 0; i<gc.edges.size(); i++){
		os<<"<"<<gc.edges[i].getX()<<","<<gc.edges[i].getY()<<">";
		if(i != gc.edges.size()-1){
			os<<" ";
		}
	}
	os<<"\t";
	for(unsigned int i = 0; i<gc.tiles.size(); i++){
		os<<"<"<<gc.tiles[i].getX()<<","<<gc.tiles[i].getY()<<">";
		if(i != gc.tiles.size()-1){
			os<<" ";
		}
	}
	os<<"\n";
	return os;
}

//input string format: id	wieght	sea	<tilex,tiley> <tilex,tiley>	<edgex,edgey> <edgex,edgey>
