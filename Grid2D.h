#ifndef GRID_2D
#define GRID_2D

#include <exception>
#include <iostream>
#include "Vector2D.h"
//Grid starts on 0 0
template <typename T>
class Grid2D{
	template <class U>
		friend std::ostream& operator<<(std::ostream& os, const Grid2D<U>& grid);

	protected:
		//2d array of the tiles. Accessing handled by functions using things hand use by vectors
		T* tile_array;
		
		unsigned int width;
		unsigned int height;
		T* getTileArray(){return tile_array;}
	public:
		Grid2D(unsigned int w, unsigned int h);
		Grid2D(const Vector2D& v);
		//pointer to the tile array stays the same. There is really no reason I can think of to ever call this(especialy because the list). On second thought, don't ever call this. I am not sure why I implemented it
		Grid2D(const Grid2D<T>& grid);
	
		inline unsigned int getWidth(){return width;}
		inline unsigned int getHeight(){return height;}
		inline unsigned int getSize(){return width*height;}
		
		inline virtual T* getElement(unsigned int x, unsigned int y);
		inline virtual T* getElement(const Vector2D& v);
		inline virtual T* operator()(unsigned int row, unsigned int column);
		inline virtual T const * operator()(unsigned int row, unsigned int column) const;
		
		inline virtual void set(T& insert, unsigned int row, unsigned int column);
		inline virtual bool isFree(const Vector2D& v);
		~Grid2D();
};

//2D Wraparound acts as a toroid instead of a plane (upon reaching the borders, it wraps around to the front
template <typename T>
class Grid2DWrap:public Grid2D<T>{
	public:
		Grid2DWrap(unsigned int w, unsigned int h) : Grid2D<T>(w, h){};
		Grid2DWrap(const Vector2D& v) : Grid2D<T>(v){};
		Grid2DWrap(const Grid2DWrap<T>& grid) : Grid2D<T>(grid){};
	
	
		inline Vector2D vectorize(int value);
	
		T* getElement(int x, int y);
		T* getElement(const Vector2D& v);
		T* operator()(int row, int column);
		T const * operator()(int row, int column) const;
		
		bool isFree(const Vector2D& v);
};


//template classes and g++ don't get along 
template <typename T> Grid2D<T>::Grid2D(unsigned int w, unsigned int h){
	width = w;
	height = h;

	tile_array = new T[width*height];
	for(unsigned int i = 0; i<width*height; i++){
		tile_array[i] = NULL;
	}
}
template <typename T> Grid2D<T>::Grid2D(const Vector2D& v){
	width = v.getX();
	height = v.getY();

	tile_array = new T[width*height];
}

template <typename T> Grid2D<T>::Grid2D(const Grid2D<T>& grid){
	tile_array = grid.getTileArray();
	
	width = grid.getWidth();
	height = grid.getHeight();
}

template <typename T> inline T* Grid2D<T>:: getElement(unsigned int x, unsigned int y){
	if(x<=width || y <= width){
		throw "getElement call out of bounds";
		return nullptr;
	}
	return &tile_array[y*width + x];
}
template <typename T> inline T* Grid2D<T>:: getElement(const Vector2D& v) {
	return getElement(v.getX(), v.getY());
}
template <typename T> inline T* Grid2D<T>::operator()(unsigned int x, unsigned int y){
	if(x<=width || y <= width){
		throw "getElement call out of bounds";
		return nullptr;
	}
	return &tile_array[y*width + x];
}
template <typename T> inline T const * Grid2D<T>::operator()(unsigned int x, unsigned int y)const{
	if(x<=width || y <= width){
		throw "getElement call out of bounds";
		return nullptr;
	}
	return &tile_array[y*width + x];
}

template<typename T> void Grid2D<T>::set(T& insert, unsigned int column, unsigned int row){
	tile_array[row*width+column] = insert;
}
template<typename T> bool Grid2D<T>::isFree(const Vector2D& v){
	if(v.getX()>=width || v.getX()<0 || v.getY()>=height || v.getY()<0){
		return false;
	}
	if(!*getElement(v)){
		return true;
	}
	return false; 
}

template <typename T> Grid2D<T>::~Grid2D(){
	delete[] tile_array;
}


template <typename T> Vector2D Grid2DWrap<T>::vectorize(int x){
	return Vector2D(x%this->width, x/this->width);
}

template <typename T>  T* Grid2DWrap<T>:: getElement(int x, int y){
	x = x%this->width;
	y = y%this->height;
	return &this->tile_array[y*this->width + x];
}
template <typename T>  T* Grid2DWrap<T>:: getElement(const Vector2D& v){
	return getElement(v.getX(), v.getY());
}
template <typename T>  T* Grid2DWrap<T>::operator()(int x, int y){
	x = x%this->width;
	y = y%this->height;
	return &this->tile_array[y*this->width + x];
}
template <typename T>  T const * Grid2DWrap<T>::operator()(int x, int y)const{
	x = x%this->width;
	y = y%this->height;

	return &this->tile_array[y*this->width + x];
}

template <typename T> std::ostream& operator<<(std::ostream& os, const Grid2D<T>& grid){
	os<<"Height: "<<grid.height<<", Width: "<<grid.width;
	for(unsigned int i =0; i<grid.width*grid.height; i++){
		if(i%grid.width == 0){
			os<<"\n";
		}
		os<<grid.tile_array[i];
	}
	os<<"\n";
	return os;
}
template<typename T> bool Grid2DWrap<T>::isFree(const Vector2D& v){
	if(!*getElement(v)){
		return true;
	}
	return false; 
}
#endif